# Vision Project

Pour : 6 employées répartie dans 3 services différents

Qui veulent : Un service de téléphonie interne

Produit : -AD qui va contenir les groupes utilisateurs / services |  Asterisk

Est un : un service d'annuaire* / un service de téléphonie IP

Qui : qui crée un annuaire en foncton du service, programme des rappelles automatiques et qui rend plus efficace la communication entre les services

A la différence de : 3CX et / ou Centrex IP qui ne sont pas libre de droit (Asterisk qui est directement issue de Linux donc libre), de plus, ils sont moins complet, il y a des fonctionnalités qui sont à ajouter et / ou à paramétrer là où dans Asterisk les doubles appellent les files d'attente, les agents d'appels, les musiques d'attente et les mises en garde d'appels sont déjà implantées, on peut également faire des conférences. 


Notre produit : permet une mise en place rapide d'un service VOIP, simple d'utiilisation pour les employés

# Risques

## Risques techniques

Problème configuration serveur - fréquence qui dépend de l'administrateur - impact important

Pannes eventuelles (logicielle ou physique ou réseau) - fréquence faible - impact important

## Risques Gestion de Projet 

Risque de réorganisation du projet - fréquence faible - impact important

Risque de manquement de la deadline - fréquence ? - impact faible

## Risques organisationnels

Risque lié au changement de la politique de l'entreprise - fréquence faible - impact important

## Risques externes 

Risque de délais non respectés par le fournisseur internet - fréquence faible - impact important

bla bla






